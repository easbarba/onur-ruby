# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

# DEPS: gcc meson ninja muon coreutils valgrind indent splint cunit

.DEFAULT_GOAL: test
RUNNER := podman

SHELL = bash -O extglob

PREFIX     ?= ${HOME}/.local/bin
NAME       = onur
VERSION    := $(shell awk -F '"' '/VERSION/ { print $2 }' lib/onur/version.rb)
BINDIR     := ./bin
IMAGES_REGISTRY := registry.gitlab.com
IMAGE_NAME := ${IMAGES_REGISTRY}/${USER}/${NAME}:${VERSION}

RM         = rm -rf

# ------------------------------------ TASKS

.PHONY: setup
setup:
	$(BINDIR)/setup

.PHONY: setup-devel
setup-devel: clean
	$(BINDIR)/setup

.PHONY: compile
compile:
	meson compile -C $(BUILDDIR)

.PHONY: all
all: clean setup compile

.PHONY: all-dev
all: clean setup compile

.PHONY: test
test:
	${RUNNER} run --rm -it  ${IMAGE_NAME}

.PHONY: clean
clean:

.PHONY: install
install:
	bundle exec rake install

.PHONY: uninstall
uninstall:
	rm ${PREFIX}/${NAME}

.PHONY: format
format:

.PHONY: lint
lint:
	bundle exec reek

.PHONY: leaks
leaks:

# ------------------------------------ ACTIONS

.PHONY: usage
usage:


.PHONY: grab
grab:
	$(BUILDDIR)/src/$(NAME) --grab

.PHONY: archive
archive:
	$(BUILDDIR)/src/$(NAME) --grab

.PHONY: filter
filter:
	$(BUILDDIR)/src/$(NAME) --grab

# ------------------------------------ ACTIONS

.PHONY: image.build
image.build:
	$(RUNNER) build -f Containerfile.dev --tag  ${IMAGE_NAME}

.PHONY: image.publish
image.publish:
	${RUNNER} push ${IMAGE_NAME}
