# frozen_string_literal: true

require_relative "lib/onur/version"

Gem::Specification.new do |spec|
  spec.name = "onur"
  spec.version = Onur::VERSION
  spec.authors = ["EAS Barbosa"]
  spec.email = ["easbarba@outlook.com"]

  spec.summary = "Easily manage multiple FLOSS repositories."
  spec.description = spec.summary
  spec.homepage = "https://gitlab.com/easbarba/onur-ruby"
  spec.required_ruby_version = ">= 3.0.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "Easily manage multiple FLOSS repositories."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  gemspec = File.basename(__FILE__)
  spec.files = IO.popen(%w[git ls-files -z], chdir: __dir__, err: IO::NULL) do |ls|
    ls.readlines("\x0", chomp: true).reject do |f|
      (f == gemspec) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git .gitlab-ci.yml appveyor Gemfile])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # -- DEPENDENCIES
  spec.add_development_dependency "rbs", "~> 3.5.1"
  spec.add_development_dependency "steep", "~> 1.7.1"
  spec.add_development_dependency 'reek', '>=6.3.0'
  spec.add_development_dependency "rspec", "~> 3.13"
  spec.add_development_dependency "rake", "~> 13.2", ">= 13.2.1"
  # spec.add_develpment_dependency "rubocop", "~> 1.64", ">= 1.64.1"
end
