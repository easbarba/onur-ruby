#!/usr/bin/env bash

# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

mkdir -pv /root/.config/onur
cp -v ./examples/* /root/.config/onur
touch /root/.config/onur/emptyfile.json
ln -sf /root/nonexistentfile /root/.config/onur/dangling_link.json
ln -sf /root/.config/onur/etc.json /root/.config/onur/etc_link.json
